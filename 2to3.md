# From Py2 to Py3

Here is a little document about migrating Progdupeupl to Python 3.

## Incompatible libraries

Here is a list of incompatible libraries, to wait to be Py3-ready or to remove
from projet dependencies :

 * crispy-forms-foundation
 * django-filter
 * django-oauth2-provider
 * django-rest-swagger
 * fabric
 * django-simple-math-captcha

This list was generated using the `caniusepython3` program available on PyPI.

## TODO

This has to be implemented when all required libraries will be availaible under
Py3, which is not the case actually.

 * Replace `from cString import StringIO` by `from io import StringIO`.
